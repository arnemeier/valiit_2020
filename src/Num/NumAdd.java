package Num;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Stack;

public class NumAdd {
    public static void main(String[] args) throws IOException {
        List<Stack> allStacks = readNums(args[0]);
        Stack<String> answer = new Stack();
        int sum = 0;
        for (int i = 0; i < 60; i++) {
            for (Stack<Integer> currentStack : allStacks) {
                sum += currentStack.pop();
            }
            answer.push(String.valueOf(sum % 10));
            sum /= 10;
        }
        String[] lastOnes = String.valueOf(sum).split("");
        for (int i = lastOnes.length - 1; i >= 0; i--) {
            answer.push(lastOnes[i]);
        }
        StringBuilder sb = new StringBuilder();
        while (!answer.isEmpty())
            sb.append(answer.pop());
        System.out.println(sb.toString());
    }

    private static Stack<Integer> stringToStack(String input) {
        Stack<Integer> nums = new Stack<>();
        for (int i = 0; i < input.length(); i++) {
            nums.add(Integer.parseInt(input.substring(i, i + 1)));
        }
        return nums;
    }

    private static List<Stack> readNums(String pathString) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(pathString));
        List<Stack> listOfStacks = new Stack<>();
        for (String line : lines) {
            listOfStacks.add(stringToStack(line));
        }
        return listOfStacks;
    }
}
