package Num;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class NumMarek {

        public static void main(String[] args) throws IOException {
            List<String> nums = readFileLines(args[0]);
            String result = "";
            int transfer = 0;
            for (int col = nums.get(0).length() - 1; col >= 0; col--) {
                int sum = transfer;
                for (int row = 0; row < nums.size(); row++) {
                    sum += Character.getNumericValue(nums.get(row).charAt(col));
                }
                transfer = sum / 10;
                int digit = sum % 10;
                result = digit + result;
            }
            if (transfer > 0) {
                result = transfer + result;
            }
            System.out.println("Summa: " + result);
        }

        private static List<String> readFileLines(String file) throws IOException {
            Path path = Paths.get(file);
            return Files.readAllLines(path);
        }


}
