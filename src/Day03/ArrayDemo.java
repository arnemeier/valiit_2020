package Day03;

public class ArrayDemo {
    public static void main(String[] args) {
        int[][] monthlyTemperatures = {
                {-7, 3},
                {-9, 7},
                {0, 5},
                {5, 12},
                {13, 15},
                {7, 18},
                {16, 23},
                {16, 17},
                {8, 11},
                {5, 8},
                {3, 7},
                {-4, 1}
        };
        System.out.println("juuni max: " + monthlyTemperatures[5][1]);

        //ex7
        int[] arvud;
        arvud = new int[5];
        arvud[0] = 1;
        arvud[1] = 2;
        arvud[2] = 3;
        arvud[3] = 4;
        arvud[4] = 5;
        System.out.println(arvud[arvud.length - 1]);

//        ex8
        String tekstid[] = new String[]{"Tallinn", "Helsinki", "Madrid", "Paris"};
        System.out.println(tekstid[1]);

//        ex9
        int[][] arvud2d = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9, 0}};
        System.out.println(arvud2d[1][2]);

//        ex10
        String[][] linnad;
        linnad = new String[3][4];
//        linnad[0] = new String[]{"Tallinn", "Tartu", "Valga", "Võru"};
//        linnad[0] = new String[4];
        linnad[0][0] = "Tallinn";
        linnad[0][0] = "Tartu";
        linnad[0][0] = "Valga";
        linnad[0][0] = "Võru";
        linnad[1] = new String[]{"Stockholm", "Uppsala", "Lund", "Köping"};
        linnad[2] = new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"};

        String[][] linnad2 = {{"Tallinn", "Tartu", "Valga", "Võru"}, {"Stockholm", "Uppsala", "Lund", "Köping"}, {"Helsinki", "Espoo", "Hanko", "Jämsä"}};

    }


}

