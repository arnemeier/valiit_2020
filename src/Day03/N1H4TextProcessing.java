package Day03;

import java.util.Scanner;

public class N1H4TextProcessing {
    public static void main(String[] args) {
//        harjutus 1
        String president1 = "Konstantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";
        String komad = ", ";
        String ja = " ja ";
        String tagumineOts = " on Eesti presidendid.";

        StringBuilder sb = new StringBuilder();
        sb.append(president1);
        sb.append(komad);
        sb.append(president2);
        sb.append(komad);
        sb.append(president3);
        sb.append(komad);
        sb.append(president4);
        sb.append(ja);
        sb.append(president5);
        sb.append(tagumineOts);
        String presidendid = sb.toString();
        System.out.println(presidendid);
//      harjutus 2
        String tekst = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";
        Scanner myScanner = new Scanner(tekst).useDelimiter("Rida: ");
//        tekst.split(". R");
        System.out.println(myScanner.next());
        System.out.println(myScanner.next());
        System.out.println(myScanner.next());
    }

}
