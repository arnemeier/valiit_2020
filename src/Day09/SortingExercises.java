package Day09;

import Bank.Account;
import Bank.AccountService;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SortingExercises {
    public static void main(String[] args) throws IOException {
        AccountService.loadAccounts("/Users/arne/dev/HelloWorldApp/Resources/kontod.txt");
        List<Account> customers = AccountService.getAccounts();

//        System.out.println("sorteerime konto jäägi järgi kahanevalt");
        Collections.sort(customers, (c1, c2) -> (int) (c2.getBalance() - c1.getBalance()));
//        System.out.println(customers);

//        System.out.println("sorteerime eesnime ja perenime järgi");
//        Collections.sort(customers, (a1, a2) -> {
//            if (a1.getEesnimi().compareTo(a2.getEesnimi()) != 0){
//                return a1.getEesnimi().compareTo(a2.getEesnimi());
//            }
//            else {
//                return a1.getPerenimi().compareTo(a2.getPerenimi());
//            }
//        });
//        System.out.println(customers);


//        System.out.println("Otsime välja rikkad kliendid (rikkad = 1000+€). Sorteerime ära ka.");

        List<Account> richCustomers = customers.stream().filter(a-> a.getBalance() > 2400).collect(Collectors.toList());
        richCustomers.sort((c1, c2) -> (int) (c2.getBalance() - c1.getBalance()));


        System.out.println(richCustomers);

    }
}
