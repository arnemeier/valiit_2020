package Day09;

import java.util.*;

public class EmployeeManagement {
    public static void main(String[] args) {
        Employee mati = new Employee("Mati", "Kask", 1500, "Eestlane");
        Employee kati = new Employee("Kati", "Tamm", 1800, "Rootslane");
        Employee priit = new Employee("Priit", "Järv", 1300, "Soomlane");
        Employee adalbert1 = new Employee("Adalbert", "Mägi", 1900, "Lätlane");
        Employee adalbert2 = new Employee("Adalbert", "Mägi", 1100, "Lätlane");

        System.out.println(adalbert1.equals(adalbert1));

        List<Employee> employees = new ArrayList<>();
        employees.add(mati);
        employees.add(kati);
        employees.add(priit);
        employees.add(adalbert1);
        employees.add(adalbert2);
//        variant 1
//        Collections.sort(employees, new EmployeeSortingInstruction());
//        variant 2, inline-implementatsioon
//        Comparator<Employee> employeeComparator = new Comparator<>() {
//            @Override
//            public int compare(Employee employee1, Employee employee2) {
//                return 0;
//            }
//        };
//        Collections.sort(employees, employeeComparator);

//        variant 3: lambda avaldis on interface'i inline-realiseerimine kompaktses vormis
//        lambda avalidst saab kasutada ainult siis, kui interface on @FunctionalInterface
//        functional interface – interface, millel on ainult 1 implementeerimist vajav meetod

        Collections.sort(employees, (employee1, employee2) -> employee2.getSalary() - employee1.getSalary());


        System.out.println(employees);


    }
}
