package Day09;

import java.util.Objects;

public class Employee implements Comparable<Employee> {
    private String firstName;
    private String lastName;
    private int salary;
    private String nationality;

    public Employee(String firstName, String lastName, int salary, String nationality) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.nationality = nationality;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return "{" +
                "name: '" + firstName + lastName + '\'' +
                ", sal: " + salary +
                ", nat: '" + nationality + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return salary == employee.salary &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(nationality, employee.nationality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, salary, nationality);
    }

    @Override
    public int compareTo(Employee another) {
//        return this.firstName.compareTo(another.firstName);
//        teistpidi
//        return this.firstName.compareTo(another.firstName) * -1;
//        if (this.salary > another.salary) {
//            return -1;
//        } else if (this.salary < another.salary) {
//            return 1;
//        } else {
//            return 0;
//        }

//        näiteid veel
//        return Integer.compare(this.salary, another.salary);
//        return this.salary < another.salary ? 1 : (this.salary > another.salary ? -1 : 0);

//        ja mastermeetod!!
        return this.salary - another.salary;

//        mitmega sorteerimise näide
//        loogika selles, et tuleb opereerida 0-casega: siis kui on null, siis tuleb
//        järgmist case'i händlida

    }
}
