package Day02;

import javax.crypto.spec.PSource;

public class N1H2IfClauses {
    public static void main(String[] args) {
        /*Koodikonstruktsioonid

Ülesanne 2: valikute konstrueerimine
Defineeri String-tüüpi muutuja ja anna talle väärtuseks "Berlin".
Konstrueeri if-lause, mis kontrolliks, kas antud muutuja väärtus on "Milano". Kui on, siis kirjuta standardväljundisse lause: "Ilm on soe." Kui mitte, siis kirjuta: "Ilm polegi kõige tähtsam!"

Ülesanne 3: if, else if, else
Defineeri muutuja, mis hoiaks täisarvulist väärtust vahemikus 1 - 5. Väärtusta see muutuja.
Kasuta "if"-"else if"-"else" konstruktsiooni, et printida ekraanile kas "nõrk", "mitterahuldav", "rahuldav", "hea", "väga hea" - vastavalt sellele, mis hinne eelpooldefineeritud muutujasse salvestatud.

Ülesanne 4: switch
Defineeri muutuja, mis hoiaks täisarvulist väärtust vahemikus 1 - 5. Väärtusta see muutuja.
Kasuta switch-konstruktsiooni, et printida ekraanile kas "nõrk", "mitterahuldav", "rahuldav", "hea", "väga hea" - vastavalt sellele, mis hinne eelpooldefineeritud muutujasse salvestatud.

Ülesanne 5: inline-if
Defineeri muutuja, mis hoiaks endas täisarvulist väärtust.
Kasutades inline-if konstruktsiooni, kirjuta standardväljundisse kas "Noor" või "Vana", vastavalt sellele, kas muutuja väärtus on suurem, kui 100 või mitte.

Ülesanne 6: inline-if
Defineeri muutuja, mis hoiaks endas täisarvulist väärtust.
Kasutades inline-if konstruktsiooni, kirjuta standardväljundisse kas "Noor", "Vana" või "Peaaegu vana", vastavalt sellele, kas inimene vanus on alla saja, üle saja või täpselt sada.
*/
        // 2
//        String linn = "Berlin";
//        if (linn.equals("Milano")){
//            System.out.println("Ilm on soe.");
//        } else {
//            System.out.println("Ilm polegi kõige tähtsam!");
//        }

        // 3
        int hinne = Integer.parseInt(args[0]);
        if (hinne == 1) {
            System.out.println("nõrk");
        } else if (hinne == 2) {
            System.out.println("mitterahuldav");
        } else if (hinne == 3) {
            System.out.println("rahuldav");
        } else if (hinne == 4) {
            System.out.println("hea");
        } else if (hinne == 5) {
            System.out.println("väga hea");
        } else {
            System.out.println("mis number see on?");
        }

        // 4

        switch (hinne){
            case 1:
                System.out.println("nõrk");
                break;
            case 2:
                System.out.println("mitterahuldav");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 5:
                System.out.println("väga hea");
                break;
            default:
                System.out.println("mis number see on?");
        }
//
//        // 5/6
//
        int vanus = 105;
        String hinnang = vanus > 100 ? "vana" : vanus == 100 ? "peaaegu vana" : "noor" ;
        System.out.println(hinnang);
    }
}
