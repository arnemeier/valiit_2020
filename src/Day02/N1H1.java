package Day02;

import java.math.BigInteger;

public class N1H1 {
    public static void main(String[] args) {
        //Defineeri muutuja, mis hoiaks väärtust 456.78.
        float f = 456.78F;
        System.out.println(f);

        //Defineeri muutuja, mis viitaks tähtede jadale "test".
        String jada = "test";
        System.out.println(jada);

        // Defineeri muutuja, mis hoiaks tõeväärtust, mis viitaks avaldise tõesusele.
        boolean jahEi = true;
        System.out.println(jahEi);

        //Defineeri kaks erinevat tüüpi muutujat, mis hoiaksid endas väärtust 'a'.
        char esimeneA = 'a';
        System.out.println(esimeneA);

        short teineA = 97;
        System.out.println((char) teineA);

        //Defineeri muutuja, mis viitaks numbrite kogumile 5, 91, 304, 405.
        int[] numbridPisi = {5, 91, 304, 405};
        System.out.println(numbridPisi);

        //Defineeri muutuja, mis viitaks numbrite kogumile 56.7, 45.8, 91.2.
        double[] komadGrupp = {56.7, 45.8, 91.2};
        System.out.println(komadGrupp[1]);

        //Defineeri muutuja, mis viitaks järgmiste väärtuste kogumile:
        //"see on esimene väärtus"
        //67
        //58.92
        Object[] samad = {"see on esimene väärtus", 67, 58.92};
        System.out.println(samad[0]);

        //Defineeri muutuja, mis viitaks väärtusele 7676868683452352345324534534523453245234523452345234523452345 ja mis ei oleks String-tüüpi.
        BigInteger pikkJama = new BigInteger("7676868683452352345324534534523453245234523452345234523452345");
        System.out.println(pikkJama);

        Long wrapperA = 5L;
        Long wrapperB = 8L;
        long wrapperSum = wrapperA + wrapperB;
        System.out.println(wrapperSum);


    }
}
