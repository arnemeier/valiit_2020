package Day06;

public class N2H1 {

    //private selleks, et meetod toimiks vaid selle klassi piires
    private static int liidaNumbreidKokku(int a, int b, int c) {
        int result = (a + b + c) / 2;
        return result;
    }

    public static void main(String[] args) {
//        System.out.println(liidaNumbreidKokku(2,3,4));
//        System.out.println(tagastaIsikukood("49403145444"));

//        test2("yks", "kaks");
//        System.out.println(addVat(132));
//        System.out.println(iseMoeldud(3,4, false)[1]);
//        printHello();
//        System.out.println(deriveGender("48004080280"));
//        validatePersonalCode("49403145444");
        validatePersonalCode("51107121760");

    }

    //    ex1
    private static boolean test(int sisend) {
        return true;
    }

    //    ex2
    private static void test2(String esimene, String teine) {

    }

    //    ex3
    private static double addVat(double kmVaba) {
        return kmVaba * 1.2;
    }

    //    ex4
    private static int[] iseMoeldud(int a, int b, boolean test) {
        int[] result = {a, b};

        return result;
    }

    //    ex5
    private static void printHello() {
        System.out.println("Tere");
    }

    //    ex6
    private static String deriveGender(String isikukood) {
        return (Integer.parseInt(isikukood.substring(0, 1))) % 2 == 0 ? "F" : "M";
    }

    //    ex7
    private static int tagastaIsikukood(String isikukood) {
        int sajand = Integer.parseInt(isikukood.substring(0, 1));
        int aasta;
        int aastaKoodist = Integer.parseInt(isikukood.substring(1, 3));

        if (isikukood == null) {
            return -1;
        } else if (isikukood.length() != 11) {
            return -1;
        }

        if (sajand == 1 || sajand == 2) {
            aasta = 1800 + aastaKoodist;
        } else if (sajand == 3 || sajand == 4) {
            aasta = 1900 + aastaKoodist;
        } else if (sajand == 5 || sajand == 6) {
            aasta = 2000 + aastaKoodist;
        } else {
            return -1;
        }
        return aasta;
    }

    //    ex8
    private static boolean validatePersonalCode(String isikukood) {
        if (isikukood == null) {
            return false;
        } else if (isikukood.length() != 11) {
            return false;
        }

        int[] esimeseAstmeKaal = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
        int[] teiseAstmeKaal = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
        int[] isikukoodNumbriga = new int[10];
        for (int i = 0; i < isikukood.length() - 1; i++) {
            isikukoodNumbriga[i] = Integer.parseInt(isikukood.substring(i, i + 1));
        }
        int summa = 0;
        int teineSumma = 0;
        for (int i = 0; i < esimeseAstmeKaal.length; i++) {
            summa += isikukoodNumbriga[i] * esimeseAstmeKaal[i];
        }

        int controlNumber = summa % 11;

        if (controlNumber == 10) {
            for (int i = 0; i < teiseAstmeKaal.length; i++) {
                teineSumma += isikukoodNumbriga[i] * teiseAstmeKaal[i];
            }
            controlNumber = teineSumma % 11;
            if (controlNumber % 11 == 10) {
                controlNumber = 0;
            }
        }

        if (controlNumber == Integer.parseInt(isikukood.substring(10, 11))) {
            System.out.printf("True: kontrollnumber: %d, isikukoodi viimane: %s", controlNumber, isikukood.substring(10, 11));
            return true;

        } else {
            System.out.printf("False: kontrollnumber: %d, isikukoodi viimane: %s", controlNumber, isikukood.substring(10, 11));

            return false;
        }

    }

}
