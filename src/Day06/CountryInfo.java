package Day06;

import javax.swing.plaf.IconUIResource;
import java.util.Arrays;

public class CountryInfo {
    public String country;
    public String capital;
    public String primeMinister;
    public String[] languages;

    public CountryInfo(){
    }

    public CountryInfo(String country, String capital, String primeMinister, String[] languages) {
        this.country = country;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.languages = languages;
    }

    @Override
    public String toString() {
        return "CountryInfo{" +
                "country = '" + country + '\'' +
                ", capital = '" + capital + '\'' +
                ", primeMinister = '" + primeMinister + '\'' +
                ", languages = " + Arrays.toString(languages) +
                '}';
    }



}
