package Day06;

public class OOPBasics {
    public static void main(String[] args) {
        Person esimene = new Person("51107121760");
//        esimene.isikukood = "51107121760";
        Person teine = new Person();
        System.out.println(esimene.getGender());
        System.out.println(esimene.getBirthYear());
        System.out.println(esimene.getBirthMonth());
        System.out.println(esimene.getBirthDayOfMonth());
    }
}

