package Day06;


public class Person {


    public String isikukood;

    public Person (){

    }

    public Person(String isikukood){
        System.out.println("hakkame sättima: ");
        this.isikukood = isikukood;

    }


    public String getBirthMonth() {
        int month = Integer.parseInt(this.isikukood.substring(3, 5));
        switch (month) {
            case 1:
                return "Jaanuar";
            case 2:
                return "Veebruar";
            case 3:
                return "Märts";
            case 4:
                return "Aprill";
            case 5:
                return "Mai";
            case 6:
                return "Juuni";
            case 7:
                return "Juuli";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "Oktoober";
            case 11:
                return "November";
            case 12:
                return "Detsember";
            default:
                return "N/A";
        }
    }

    public int getBirthDayOfMonth() {
        return Integer.parseInt(this.isikukood.substring(5, 7));
    }

    public int getBirthYear() {
        int sajand = Integer.parseInt(this.isikukood.substring(0, 1));
        int aasta;
        int aastaKoodist = Integer.parseInt(this.isikukood.substring(1, 3));

        if (this.isikukood == null) {
            return -1;
        } else if (this.isikukood.length() != 11) {
            return -1;
        }

        if (sajand == 1 || sajand == 2) {
            aasta = 1800 + aastaKoodist;
        } else if (sajand == 3 || sajand == 4) {
            aasta = 1900 + aastaKoodist;
        } else if (sajand == 5 || sajand == 6) {
            aasta = 2000 + aastaKoodist;
        } else {
            return -1;
        }
        return aasta;
    }

    public String getGender() {
        return (Integer.parseInt(this.isikukood.substring(0, 1))) % 2 == 0 ? "F" : "M";
    }


}
