package Day06;

import java.util.Arrays;

public class CountryApp {
    public static void main(String[] args) {

        String[][][] riigid = {
                {{"Estonia", "Tallinn", "Jüri Ratas"}, {"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"}},
                {{"Latvia", "Riga", "Arturs Krišjānis Kariņš"}, {"Latvian", "Russian", "Belarusian", "Ukrainian", "Polish"}},
                {{"Lithuania", "Vilnius", "Saulius Skvernelis"}, {"lithuanian"}},
                {{"Finland", "Helsinki", "Sanna Marin"}, {"finnish"}},
                {{"Sweden", "Stockholm", "Stefan Löfven"}, {"swedish"}},
                {{"Norway", "Oslo", "Erna Solberg"}, {"norwegian"}},
                {{"Denmark", "Copenhagen", "Mette Frederiksen"}, {"danish"}},
                {{"Russia", "Moscow", "Mikhail Mishustin"}, {"russian"}},
                {{"Germany", "Berlin", "Angela Merkel"}, {"german"}},
                {{"France", "Paris", "Édouard Philippe"}, {"french"}}
        };
//
//        for (String [][] riik : riigid) {
//            System.out.println(riik[0][2]);
//            CountryInfo
//
//        }




        CountryInfo estonia = new CountryInfo();
//        estonia.capital = riigid[0][0][1];
//        estonia.country = riigid[0][0][0];
//        estonia.primeMinister = riigid[0][0][2];
//        estonia.languages = riigid[0][1];
//        System.out.println(estonia);

        estonia.capital = "Tallinn";
        estonia.primeMinister = "Jüri Ratas";
        estonia.country = "Estonia";
        estonia.languages = new String []{"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"};

        CountryInfo latvia = new CountryInfo("Latvia", "Riga", "Arturs Krišjānis Kariņš",
                new String[]{"Latvian", "Russian", "Belarusian", "Ukrainian", "Polish"});

        CountryInfo[] baltics = new CountryInfo[2];
        baltics[0] = estonia;
        baltics[1] = latvia;

        System.out.println(Arrays.toString(baltics));
    }
}
