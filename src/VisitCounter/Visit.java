package VisitCounter;

public class Visit {
    private String date;
    private int amount;

    public Visit(String inputString){
        String[] visitList = inputString.split(", ");
        this.date = visitList[0];
        this.amount = Integer.parseInt(visitList[1]);
    }

    public String getDate() {
        return date;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "{\n" +
                "kuupäev: '" + date + '\'' +
                ", külastajaid: " + amount +
                '}';
    }
}
