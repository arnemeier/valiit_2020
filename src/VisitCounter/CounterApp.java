package VisitCounter;

import Bank.Account;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

public class CounterApp {
    public static void main(String[] args) throws IOException {
        List<Visit> visits = readFile(args[0]);
        visits.sort((v1, v2) -> v2.getAmount() - v1.getAmount());

        visits.stream().max(Comparator.comparing(Visit::getAmount)).get();

        visits.stream().max((v1, v2) -> {
            int highest = 0;
            if (v1.getAmount() > highest) {
                highest = v1.getAmount();
            }
            return highest;
        });

//        System.out.println(visits.stream().max(Comparator.comparing(v->v.getAmount())).get().getDate());

        System.out.println(visits);
//        System.out.println(visits.get(0).getDate());
    }


    private static List<Visit> readFile(String currentPath) throws IOException {
        Path path = Paths.get(currentPath);
        List<String> lines = Files.readAllLines(path);
        List<Visit> visits = new ArrayList<>();

        for (String line : lines) {
            visits.add(new Visit(line));
        }

        return visits;

//        System.out.println("Sample 4: read file as list");
//        for (String line : lines) {
//            System.out.println(line);
//        }
    }

}
