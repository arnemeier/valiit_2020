package Pranglija;

import java.sql.ClientInfoStatus;
import java.util.*;

public class Prangel {
    private static final int GUESS_COUNT = 2;
    private static List<GameResult> gameLog = new ArrayList<>(); //nimi, kestus, min, max
    private static Scanner cmdScanner = new Scanner(System.in);

    private static int getIntFromConsole() {
        while (true){
            try {
                return   Integer.parseInt(cmdScanner.nextLine());
            } catch (NumberFormatException e){
                System.out.println("Palun sisesta number: ");
//                getIntFromConsole();

            }
        }

//        int number = 0;
//        try {
//            return Integer.parseInt(cmdScanner.nextLine());
//        } catch (NumberFormatException e) {
//        }
//        return number;
    }

    public static void main(String[] args) {
        System.out.println("_________ Pranglija __________");
        System.out.println("______________________________\n");

        while (true) {
            System.out.println("Tere, mis su nimi on?");
            String nimi = cmdScanner.nextLine();

            System.out.println("Sisesta genereeritavate arvude miinimumväärtus: ");
            int minGuess = getIntFromConsole();
            System.out.println("Sisesta genereeritavate arvude maksimumväärtus: ");
            int maxGuess;
            maxGuess = getIntFromConsole();

            long startTime = System.currentTimeMillis();

//        System.out.println(startTime);

            int i = 0;
            for (; i < GUESS_COUNT; i++) {
                int esimeneNr = (int) (Math.random() * (maxGuess - minGuess + 1) + minGuess);
                int teineNr = (int) (Math.random() * (maxGuess - minGuess + 1) + minGuess);
                System.out.println(String.format("%d + %d = ?", esimeneNr, teineNr));

                int test = getIntFromConsole();

                if (test != (esimeneNr + teineNr)) {
                    System.out.println("Mäng läbi!");
                    break;
                }
            }
            if (i == GUESS_COUNT) {
                System.out.println("Võitsid mängu!");
                long endTime = System.currentTimeMillis();
                double elapsedTime = endTime - startTime;
                elapsedTime /= 1000.0;
                System.out.println("Mängu kestus: " + (endTime - startTime));
                String result = nimi + ", " + Long.toString(endTime - startTime) + ", " + minGuess + ", " + maxGuess;
//                result = String.format("%s, %d sekundit, %d-%d", nimi, (endTime - startTime), minGuess, maxGuess );
                GameResult tulemus = new GameResult();
                tulemus.name = nimi;
                tulemus.time = elapsedTime;
                tulemus.range = minGuess + "-" + maxGuess;
                gameLog.add(tulemus);
                gameLog.sort((resultA, resultB) -> {
                    if (resultA.time > resultB.time) {
                        return 1; // vaheta A ja B järjekord
                    } else {
                        return -1; // on järjekord juba ok
                    }

                });

                for (GameResult res : gameLog) {
                    System.out.println("______________________________");
                    System.out.println("__________HIGSCORES!__________");
                    System.out.println("______________________________");
                    System.out.println("Nimi:\t\t" + res.name);
                    System.out.println("Aeg:\t\t" + res.time + " sek");
                    System.out.println("Vahemik:\t" + res.range);
                }

            } else {
                System.out.println("Proovi veel!");
            }

            System.out.println("Mäng läbi, kuidas edasi? (0: exit, 1: uuesti)");
            int userCommand = getIntFromConsole();
            if (userCommand == 0) {
                break;
            }
        }


        System.out.println("");

        System.out.println("\n__________ Edetabel __________");
        System.out.println("______________________________");
//        System.out.println(gameLog);
        System.out.println("\n_________ Head aega! _________");
        System.out.println("______________________________");

    }
}
