package Pranglija;

public class GameResult {
    public String name;
    public double time;
    public String range;

    @Override
    public String toString() {
        return "GameResult{" +
                "name='" + name + '\'' +
                ", time=" + time +
                ", range='" + range + '\'' +
                '}';
    }



}
