package Testülesanded;

public class WealthyCountry {
    private String name;
    private long gdp;
    private long population;
    private long economySize;

    public String getName() {
        return name;
    }

//    public long getGdp() {
//        return gdp;
//    }
//
//    public long getPopulation() {
//        return population;
//    }

    public long getEconomySize(){
        return gdp * population;
    }


    public WealthyCountry() {
    }

    public WealthyCountry(String name, long gdp, long population) {
        this.name = name;
        this.gdp = gdp;
        this.population = population;
//        this.economySize = this.gdp * this.population;
    }

    @Override
    public String toString() {
        return "Country name: " + name + ", gdp: " + gdp +
                ", population: " + population +
                ", economySize: " + this.getEconomySize();
    }
}
