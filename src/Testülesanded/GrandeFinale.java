package Testülesanded;

import java.util.ArrayList;
import java.util.List;

public class GrandeFinale {
    public static void main(String[] args) {

        double[] komadegaNumbrid = {3.5, 6.9, 1.8, 9.7, 45, 99};
        //juhuks kui on soov konsoolile väljastada
//        System.out.println("Ex 1");
//        Arrays.stream(komadegaNumbrid).forEach(System.out::println);

        System.out.println("Ex 2");
        int[] wages = {1700, 1500, 2320, 980, 1320, 1500, 1550, 1790};

        System.out.printf("Keskmine makstud tulumaks on: %.2f",calculateAverageIncomeTax(wages));
        System.out.println();

        System.out.println("Ex 3");
        String firstExample = "Meelas mõmm maiustas mäe otsas maasikatega.";
        String secondExample = "Kuri karu järas jõe ääres juurikaid.";
        //võib testiks vahetada firstExample / secondExample
        System.out.printf("Tsenseeritud string on: %s", censorMs(firstExample));
        System.out.println();

        System.out.println("Ex 4");
        //copy-paste
        String inputStringFromGoogleDocs =
                "Switzerland\t83716\t\t8654622\n" +
                "Singapore\t\t63987\t\t5850342\n" +
                "Ireland\t\t77771\t\t4937786\n" +
                "Iceland\t\t67037\t\t341243\n" +
                "Macau\t\t81151\t\t649335\n" +
                "Denmark\t\t59795\t\t5792202\n" +
                "Qatar\t\t69687\t\t2881053\n" +
                "United States\t65111\t\t331002651\n" +
                "Norway\t\t77975\t\t5421241\n";

        List<WealthyCountry> wcList = parseWealthyCountries(inputStringFromGoogleDocs);
        wcList.sort((c1,c2)-> Long.compare(c2.getEconomySize(), c1.getEconomySize()));

        WealthyCountry wcBiggest = wcList.get(0);
        WealthyCountry wcSmallest = wcList.get(wcList.size()-1);

        System.out.printf("a) Kõige suurema majandusega riik: %s", wcBiggest.getName());
        System.out.println();
        System.out.println("(" + wcBiggest + ")");
        System.out.printf("b) Kõige väiksema majandusega riik: %s", wcSmallest.getName());
        System.out.println();
        System.out.println("(" + wcSmallest + ")");
        System.out.printf("c) Keskmine majanduse suurus: %d", averageEconomy(wcList));

    }

    public static double calculateAverageIncomeTax(int[] submittedWages) {
        int sum = 0;
        for (int wage : submittedWages) {
            sum += wage;
        }
        System.out.println(sum);
        return sum * 0.2 / submittedWages.length;
    }

    public static String censorMs(String inputString){
        String result = "";
        char[] inputChars = inputString.toCharArray();
        for (char currentChar: inputChars) {
            result += (currentChar == 'm' || currentChar == 'M') ? '#' : currentChar;
        }
        return result;
        //alternatiivne lahendus
    }

    //copy-paste parser laiskadele
    public static List<WealthyCountry> parseWealthyCountries (String input){
        List<WealthyCountry> wCountries = new ArrayList<>();
        for (String str:input.split("\n")) {
            wCountries.add(new WealthyCountry( str.split("\t++")[0], Long.parseLong(str.split("\t++")[1]), Long.parseLong(str.split("\t++")[2])));
        }
        return wCountries;
    }

    public static long averageEconomy(List<WealthyCountry> inputWealthyCountries){
        long result = 0;
        for (WealthyCountry wealthyCountry : inputWealthyCountries) {
            result += wealthyCountry.getEconomySize();
        }
        return result / inputWealthyCountries.size();
    }

}
