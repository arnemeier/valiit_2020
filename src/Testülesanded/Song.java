package Testülesanded;

import java.util.List;

public class Song {
    private String title;
    private String artist;
    private String album;
    private int released;
    private List<String> genres;
    private String lyrics;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public int getReleased() {
        return released;
    }

    public void setReleased(int released) {
        this.released = released;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public Song(String title, String artist, String album, int released, List<String> genres, String lyrics) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.released = released;
        this.genres = genres;
        this.lyrics = lyrics;
    }

    @Override
    public String toString() {
        return "Song title: " + title + '\n' +
                "Artist: " + artist + '\n' +
                "Album: " + album + '\n' +
                "Release year: " + released + "\n" +
                "Genres: " + genres + "\n" +
                "Lyrics: \n" + lyrics + "\n" +
                "__________________";
    }
}
