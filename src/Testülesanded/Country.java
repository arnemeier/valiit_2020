package Testülesanded;

public class Country {
    private String name;
    private int gdp;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGdp() {
        return gdp;
    }

    public void setGdp(int gdp) {
        this.gdp = gdp;
    }

    public Country() {
    }

    public Country(String name, int gdp) {
        this.name = name;
        this.gdp = gdp;
    }

    @Override
    public String toString() {
        return "GDP: " + gdp + ", country: " + name;
    }
}

