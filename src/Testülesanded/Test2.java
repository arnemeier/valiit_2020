package Testülesanded;

//10:03

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class Test2 {

    public static void main(String[] args) {

//        variant A
//        int[] numbers = {3, 6, 1, 9, 45, 99};
//        System.out.println("Ex 1");
//        for (int num : numbers) {
//            System.out.printf("Number massiivis: %d\n", num);
//        }

        String numbersStringFromGoogleDocs = "3, 6, 1, 9, 45, 99";
        int[] numbers = Stream.of(numbersStringFromGoogleDocs.split(", ")).mapToInt(Integer::parseInt).toArray();
        Arrays.stream(numbers).forEach(System.out::println);

        System.out.println();

        int[] wages = {1700, 1500, 2320, 980, 1320, 1500, 1550, 1719};
        System.out.println("Ex 2");
        System.out.printf("Total income tax for given wages: %d", calculateTotalIncomeTax(wages));
        System.out.println();
        System.out.println();

        System.out.println("Ex 3");
        String mTestOne = "Meelas mõmm maiustas mäe otsas maasikatega.";
        String mTestTwo = "Kuri karu järas jõe ääres juurikaid.";
        System.out.printf("The count of letters 'm' or 'M' is %d", countMs(mTestOne));
        System.out.println();
        System.out.println();

        System.out.println("Ex 4");

        String stringFromGoogleDocs = "Switzerland\t83716\n" +
                "Singapore\t\t63987\n" +
                "Ireland\t\t77771\n" +
                "Iceland\t\t67037\n" +
                "Macau\t\t81151\n" +
                "Denmark\t\t59795\n" +
                "Qatar\t\t69687\n" +
                "United States\t65111\n" +
                "Norway\t\t77975\n";

        List<Country> countries1 = parseCountries(stringFromGoogleDocs);
        countries1.sort((c1, c2) -> c2.getGdp() - c1.getGdp());
        for (Country c : countries1) {
            System.out.println(c);
        }
    }

    //10:05
    //Remark: will fail with more precise numbers. Double for that case, decimal for banks.
    public static int calculateTotalIncomeTax(int[] input) {
////        variant 1
//        int result = 0;
//        for (int element : input) {
//            result += element * 0.2;
//        }
//        return result;
        return (int) (Arrays.stream(input).sum() * 0.2);

    }

    //10:11
    public static int countMs(String inputString) {
//    variant 1
//        int result = 0;
//        char[] letters = inputString.toLowerCase().toCharArray();
//        for (char letter:letters) {
//            if (letter == 'm'){
//                result += 1;
//            }
//        }
//        return result;

//        variant 2
        return  (int) inputString.toLowerCase().chars().filter(i -> i == 'm').count();


    }

    //10:19

    //    10:32 – finiš esimene
//    10:44 - refinery start
    public static List<Country> parseCountries(String input) {
        List<Country> countries = new ArrayList<>();
        for (String str : input.split("\n")) {
            countries.add(new Country(str.split("\t++")[0], Integer.parseInt(str.split("\t++")[1])));
        }
        return countries;
    }


}
