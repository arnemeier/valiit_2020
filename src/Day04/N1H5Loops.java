package Day04;

public class N1H5Loops {
    public static void main(String[] args) {
//        ex 1
        int limit = 100;
        int start = 1;
//        while (start <= limit){
//            System.out.println(start);
//            start++;
//        }

//        ex2
//        for (int i = 1; i <= 100; i++) {
//            System.out.println(i);
//        }

//        ex3
//        int[] kymme = new int[]{1,2,3,4,5,6,7,8,9,10};
//        for (int nr: kymme) {
//            System.out.println(nr);
//        }
//        ex4
//        for (int i = 1; i <= 100; i++) {
//            if (i % 3 == 0){
//                System.out.println(i);
//            }
//        }
//        vs
//        for (int i = 3; i < 100; i+=3) {
//            System.out.println(i);
//        }

//        ex5

//        String[] bandid = new String[] {"Sun", "Metsatöll", "Queen", "Metallica"};
//        for (int i = 0; i < bandid.length; i++) {
//            if (i != bandid.length-1){
//                System.out.print(bandid[i] + ", ");
//            }
//            else {
//                System.out.print(bandid[i]);
//            }
//        }

//        ex6
//        String[] bandid = new String[] {"Sun", "Metsatöll", "Queen", "Metallica"};
//        for (int i = bandid.length-1; i >= 0; i--) {
//            if (i != 0){
//                System.out.print(bandid[i] + ", ");
//            }
//            else {
//                System.out.print(bandid[i]);
//            }
//        }

//            ex7
//        String[] numbridLiteral = {"null", "yks", "kaks", "kolm", "neli", "viis", "kuus", "seitse", "kaheksa", "yheksa"};
////        System.out.println(args[1]);
////        System.out.println(Integer.parseInt(args[1]));
//        int index = 0;
//        for (String argument : args) {
//            System.out.print(numbridLiteral[Integer.parseInt(argument)]);
//            if (index != args.length - 1) {
//                System.out.print(", ");
//            }
//            index++;
//        }

//        ex7 vol2

//        for (int i = 0; i < args.length; i++) {
////            System.out.print(args[i]);
//            switch (args[i]){
//                case "1":
//                    System.out.print("yks");
//                    break;
//                case "2":
//                    System.out.print("kaks");
//                    break;
//                case "3":
//                    System.out.print("kolm");
//                    break;
//                case "4":
//                    System.out.print("neli");
//                    break;
//                case "5":
//                    System.out.print("viis");
//                    break;
//                case "6":
//                    System.out.print("kuus");
//                    break;
//                case "7":
//                    System.out.print("seitse");
//                    break;
//                case "8":
//                    System.out.print("kaheksa");
//                    break;
//                case "9":
//                    System.out.print("yheksa");
//                    break;
//                case "0":
//                    System.out.print("null");
//                    break;
//            }
//            if (i != args.length-1){
//                System.out.print(", ");
//            }
//        }

//        ex7 vol 3

//        for (int i = 0; i < args.length; i++) {
////            System.out.print(args[i]);
//
//            if (args[i].equals("0")){
//                System.out.print("null");
//            } else if (args[i].equals("1")){
//                System.out.print("yks");
//            } else if (args[i].equals("2")){
//                System.out.print("kaks");
//            } else if (args[i].equals("3")){
//                System.out.print("kolm");
//            } else if (args[i].equals("4")){
//                System.out.print("neli");
//            } else if (args[i].equals("5")){
//                System.out.print("viis");
//            } else if (args[i].equals("6")){
//                System.out.print("kuus");
//            } else if (args[i].equals("7")){
//                System.out.print("seitse");
//            } else if (args[i].equals("8")){
//                System.out.print("kaheksa");
//            } else if (args[i].equals("9")){
//                System.out.print("yheksa");
//            }
//            if (i != args.length - 1) {
//                System.out.print(", ");
//            }
//        }


//        ex8

        double rando;
        do {
            System.out.println("tere");
//            rando = Math.random();
        } while (Math.random() < 0.5);

    }
}
