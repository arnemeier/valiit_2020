package Day07.Koer;

public class DogApp {
    public static void main(String[] args) {
        Dog eestiKoer = new EstonianDog("eesti", "pauka");
        System.out.println(eestiKoer);
        eestiKoer.bark();

        Dog veneKoer = new RussianDog("vene", "sharik");
        System.out.println(veneKoer);
        veneKoer.bark();

        Dog saksaKoer = new GermanDog("saksa", "bello");
        System.out.println(saksaKoer);
        saksaKoer.bark();
    }
}
