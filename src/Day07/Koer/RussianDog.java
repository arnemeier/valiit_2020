package Day07.Koer;

public class RussianDog extends Dog{

    public RussianDog(String rahvus, String name) {
        super(rahvus, name);
    }

    @Override
    public void bark() {
        System.out.println("gav-gav");
    }
}
