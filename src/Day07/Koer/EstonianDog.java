package Day07.Koer;

public class EstonianDog extends Dog {

    public EstonianDog(String rahvus, String name) {
        super(rahvus, name);
    }

    @Override
    public void bark() {
        System.out.println("auh-auh");
    }
}
