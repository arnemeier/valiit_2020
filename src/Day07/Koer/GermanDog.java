package Day07.Koer;

public class GermanDog extends Dog {


    public GermanDog(String rahvus, String name) {
        super(rahvus, name);
    }

    @Override
    public void bark() {
        System.out.println("wuff-wuff");
    }
}
