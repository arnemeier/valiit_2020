package Day07.Koer;

public abstract class Dog {
    private String rahvus;
    private String name;

    public Dog(String rahvus, String name) {
        this.rahvus = rahvus;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getRahvus() {
        return rahvus;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRahvus(String rahvus) {
        this.rahvus = rahvus;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "rahvus='" + rahvus + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public abstract void bark();
}
