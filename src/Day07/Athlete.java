package Day07;

public abstract class Athlete {
    public String firstName;
    public String lastName;
    public int age;
    public String gender;
    public int pikkus;
    public int kaal;

    public Athlete(String firstName, String lastName, int age, String gender, int pikkus, int kaal) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
        this.pikkus = pikkus;
        this.kaal = kaal;
    }



    //    abstraktne klass sunnib hiljem overridima meetodi
    public abstract void perform();


}
