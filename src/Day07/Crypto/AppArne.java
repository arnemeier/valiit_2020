package Day07.Crypto;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class AppArne {
    private static Map<String, String> alfabeetMap;
    private static Map<String, String> alfabeetDecrMap;
    public static void main(String[] args) throws IOException {
        System.out.println("##############################");
        System.out.println("#########     App     ########");
        System.out.println("##############################\n");


        String alphabetFilePath = "/Users/arne/dev/HelloWorldApp/Resources/alfabeet.txt";
        alfabeetMap = getAlphabet(alphabetFilePath);

        String inputString = modifyInputString(args);

        String output = getEncryptedString(alfabeetMap, inputString);

        System.out.printf("Sisend: %s\nVäljund: %s\n", inputString, output);

        System.out.println("_______________________");

        String reInput = "FDHŽ.PQ.RÜHŽ.ÜSS";
        String reOutput = "";




        

    }

    private static String modifyInputString(String[] args) {
        String inputString = "";

        for (String arg : args) {
            inputString += arg + " ";
        }

        inputString = inputString.substring(0, inputString.length() - 1);
        return inputString;
    }

    private static String getEncryptedString(Map<String, String> alfabeetMap, String inputString) {
        String output = "";
        for (int i = 0; i < inputString.length(); i++) {
            output += alfabeetMap.get(inputString.substring(i, i + 1));
        }
        return output;
    }

    private static Map<String, String> getAlphabet(String alphabetFilePath) throws IOException {
        List<String> alfabeetLines = Files.readAllLines(Paths.get(alphabetFilePath));

        Map<String, String> alfabeetMap = new HashMap<>();
        for (String rida : alfabeetLines) {
            alfabeetMap.put(rida.split(", ")[0], rida.split(", ")[1]);
        }
        return alfabeetMap;
    }
}
