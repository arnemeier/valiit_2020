package Day07.Crypto_Marek;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class App {
    private static List<String> alphabetLines;
    private static Map<String, String> encryptionAlphabet;
    private static Map<String, String> decryptionAlphabet;

    public static void main(String[] args) throws IOException {
        System.out.println("-----------------------------------");
        System.out.println("-     VALI IT SPIOONIRAKENDUS     -");
        System.out.println("-----------------------------------");

        // Alfabeetide loomine
        String alphabetFilePath = "resources/alfabeet.txt";
        alphabetLines = getAlphabetLines(alphabetFilePath);
        encryptionAlphabet = getEncryptionAlphabet();
        decryptionAlphabet = getDecryptionAlphabet();

        // Krüpteerimine (encryption)
        String message = args[0];
        String encryptedMessage = encrypt(message);
        System.out.println("Krüpteeritud sõnum: " + encryptedMessage);

        // Dekrüpteerimine (decryption)
        String decrmessage = args[0];
        String decryptedMessage = decrypt(decrmessage);
        System.out.println("Dekrüpteeritud sõnum: " + decryptedMessage);



    }

    private static String encrypt(String message) {
        String encryptedMessage = "";
        for (String c : message.split("")) {
            String encryptedChar = encryptionAlphabet.containsKey(c.toUpperCase()) ?
                    encryptionAlphabet.get(c.toUpperCase()) : c.toUpperCase();
            encryptedMessage += encryptedChar;
        }
        return encryptedMessage;
    }

    private static String decrypt(String message) {
        String decryptedMessage = "";
        for (String c : message.split("")) {
            String decryptedChar = decryptionAlphabet.containsKey(c.toUpperCase()) ?
                    decryptionAlphabet.get(c.toUpperCase()) : c.toUpperCase();
            decryptedMessage += decryptedChar;
        }
        return decryptedMessage;
    }

    private static Map<String, String> getEncryptionAlphabet() throws IOException {
        // List: "M, R" | Map: "M" -> "R"
        Map<String, String> alphabetMap = new HashMap<>();
        for (String line : alphabetLines) {
            String[] lineParts = line.split(", ");
            alphabetMap.put(lineParts[0], lineParts[1]);
        }
        return alphabetMap;
    }
    private static Map<String, String> getDecryptionAlphabet() throws IOException {
        // List: "M, R" | Map: "M" -> "R"
        Map<String, String> alphabetMap = new HashMap<>();
        for (String line : alphabetLines) {
            String[] lineParts = line.split(", ");
            alphabetMap.put(lineParts[1], lineParts[0]);
        }
        return alphabetMap;
    }

    private static List<String> getAlphabetLines(String alphabetFilePath) throws IOException {
        return Files.readAllLines(Paths.get(alphabetFilePath));
    }
}
