package Bank;

public class Account {
    private final String eesnimi;
    private final String perenimi;
    private final String konto;
    private double balance;

    public Account(String accountCsv){
        String[] csv = accountCsv.split(", ");
        this.eesnimi = csv[0];
        this.perenimi = csv[1];
        this.konto = csv[2];
        this.balance = Double.parseDouble(csv[3]);
    }

    public String getEesnimi() {
        return eesnimi;
    }

    public String getPerenimi() {
        return perenimi;
    }

    public String getKonto() {
        return konto;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }


    @Override
    public String toString() {
        return "Kodanik: " + eesnimi + ' ' + perenimi +
                ", kontonumber: " + konto +
                ", kontojääk: " + balance;
    }
}
