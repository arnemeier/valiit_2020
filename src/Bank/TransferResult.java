package Bank;

public class TransferResult {
    private static boolean isSuccess;
    private static String message;
    private static Account fromAccount;
    private static Account toAccount;

    public TransferResult(boolean isSuccess, String message) {
        this.isSuccess = isSuccess;
        this.message = message;
    }
    public TransferResult(boolean isSuccess, String message, Account fromAccount, Account toAccount) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    @Override
    public String toString() {
        return "TransferResult{" +
                "isSuccess=" + isSuccess +
                ", message='" + message + '\'' +
                ", fromAccount=" + fromAccount +
                ", toAccount=" + toAccount +
                '}';
    }
}
