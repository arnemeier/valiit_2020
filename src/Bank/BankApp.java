package Bank;

import java.io.IOException;
import java.util.Scanner;

public class BankApp {
    public static void main(String[] args) throws IOException {

        AccountService.loadAccounts("/Users/arne/dev/HelloWorldApp/Resources/kontod.txt");

//        kontonumbri otsing test
//        Account searchResultAccount = AccountService.findAccount("758818991");
//        System.out.println(searchResultAccount != null ? searchResultAccount : "No matches!");

//        nime otsing test
//        Account searchNameResultAccount = AccountService.findAccount("Daryl", "Davis");
//        System.out.println(searchNameResultAccount != null ? searchNameResultAccount : "No matches!");

//        transfer test
//        TransferResult tulemus = AccountService.transfer("758818991", "653797240", 50);
//        System.out.println(tulemus);
//        System.out.println(searchResultAccount != null ? searchResultAccount : "No matches!");
//        System.out.println(searchNameResultAccount != null ? searchNameResultAccount : "No matches!");
//        System.out.println(searchResultAccount);

        System.out.println("##############################################");
        System.out.println("#              TÜRISALU PANK                 #");
        System.out.println("#                                            #");
        System.out.println("# SYNTAX:                                    #");
        System.out.println("# BALANCE <ACCOUNT>                          #");
        System.out.println("# BALANCE <FIRST NAME> <LAST NAME>           #");
        System.out.println("# TRANSFER <FROM ACCOUNT> <TO ACCOUNT> <SUM> #");
        System.out.println("#                                            #");
        System.out.println("##############################################");

        boolean exit = false;
        Scanner scanner = new Scanner(System.in);

        while (exit != true) {
            System.out.printf("Insert choice: ");
            String input = scanner.nextLine();
            String[] inputString = input.split(" ");

            if (inputString[0].equalsIgnoreCase("transfer")) {
                if (inputString.length != 4) {
                    System.out.println("Error, incorrect number of arguments");
                } else {

                    Double sum = getSumFromText(inputString[3]);

                    TransferResult tulemus = AccountService.transfer(inputString[1], inputString[2], sum);
                    displayTransferResult(tulemus);

                }

            } else if (inputString[0].equalsIgnoreCase("balance")) {
                if (inputString.length == 1) {
                    System.out.println("Too few arguments!");
                } else if (inputString.length == 2) {
                    Account searchedAcc = AccountService.findAccount(inputString[1]);

                    displayAccount(searchedAcc);
                } else if (inputString.length == 3) {
                    Account searchedAcc = AccountService.findAccount(inputString[1], inputString[2]);
                    displayAccount(searchedAcc);
                } else {
                    System.out.println("Too many arguments!");
                }

            } else if (inputString[0].equalsIgnoreCase("exit")) {
                exit = true;
                System.out.println("Pangas käidud! Tšau!");
            } else {
                System.out.println("Unknown command!");
            }
        }


    }

    private static double getSumFromText(String toBeDouble) {
        try {
             return Double.parseDouble(toBeDouble);
        } catch (NumberFormatException e){
        }
        return 0;
    }

    private static void displayTransferResult(TransferResult tulemus) {
        if (tulemus.isSuccess()) {
            System.out.println(tulemus.getMessage());
            System.out.println("Maksja:");
            displayAccount(tulemus.getFromAccount());
            System.out.println("------------------------------------");
            System.out.println("Saaja:");
            displayAccount(tulemus.getToAccount());
            System.out.println("------------------------------------");

        } else {
            System.out.println("Ülekanne ebaõnnestus");
            System.out.println("Veateade:" + tulemus.getMessage());
        }
    }

    private static void displayAccount(Account account) {
        if (account != null) {
            System.out.println("------------------------------------");
            System.out.println("KONTO ANDMED");
            System.out.printf("NIMI:\t\t%s %s\n", account.getEesnimi(), account.getPerenimi());
            System.out.printf("KONTO:\t\t%s\n", account.getKonto());
            System.out.printf("JÄÄK:\t\t%.2f EUR\n", account.getBalance());
        } else {
            System.out.println("------------------------------------");
            System.out.println("Kontot ei leitud!");
        }


    }
}
