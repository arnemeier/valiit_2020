package Bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AccountService {
    private static List<Account> accounts = new ArrayList<>();

    public static List<Account> getAccounts() {
        return accounts;
    }

    public static void loadAccounts(String accountFilePath) throws IOException {
        List<String> accountCsvs = Files.readAllLines(Paths.get(accountFilePath));
        for (String csv : accountCsvs) {
            accounts.add(new Account(csv));
        }
//        System.out.println(accounts);

//        lambda näide
        accountCsvs.stream().map(s -> new Account(s)).collect(Collectors.toList());
//        või nii
        accountCsvs.stream().map(Account::new).collect(Collectors.toList());
    }

    public static Account findAccount(String kontonumber) {
        for (Account currentAcc:accounts) {
            if (currentAcc.getKonto().equals(kontonumber)){
                return currentAcc;
            }
        }
        return null;
    }

    public static Account findAccount(String eesnimi, String perenimi){
        for (Account currentAcc:accounts) {
            if (currentAcc.getEesnimi().equalsIgnoreCase(eesnimi) && currentAcc.getPerenimi().equalsIgnoreCase(perenimi)){
                return currentAcc;
            }
        }
        return null;

        //        näiteks ka lambda avaldis
//        return accounts.stream()
//                .filter(acc -> acc.getEesnimi().equalsIgnoreCase(eesnimi) && acc.getPerenimi().equalsIgnoreCase(perenimi))
//                .findFirst().orElse(null);
    }

    public static TransferResult transfer(String fromAccount, String toAccount, double sum){
        Account from = findAccount(fromAccount);
        Account to = findAccount(toAccount);
        TransferResult currentRes;

        if (from == null){
            return new TransferResult(false, "Wrong account number: from");
        } else if (to == null){
            return new TransferResult(false, "Wrong account number: to");
        } else if (from.getBalance()<sum) {
            return new TransferResult(false, "Not enough funds!");
        } else if (sum <= 0){
            return new TransferResult(false, "Negative sums or zero not allowed");
        } else {
            from.setBalance(from.getBalance()-sum);
            to.setBalance(to.getBalance()+sum);
            return new TransferResult(true, "Success!", from, to);
        }

    }




}
