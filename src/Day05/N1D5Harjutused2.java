package Day05;

import java.lang.reflect.Array;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.*;

public class N1D5Harjutused2 {
    public static void main(String[] args) {

//        ex1
//        String asi = "";
//        for (int k = 0; k < 9; k++) {
//            asi += "#+";
//        }
//
//        for (int i = 0; i < 8; i++) {
////            if ((i % 2) == 0){
////                System.out.println(asi + "#");
////            } else {
////                System.out.println("+" + asi);
////            }
//            System.out.println( (i%2 == 0) ? asi + "#" : "+" + asi);
////
//        }

//        ex2

//        for (int i = 0; i < 6; i++) {
//            for (int j = 0; j < i; j++) {
//                System.out.print("_");
//            }
//            System.out.println("#");
//        }

//        ex3 ok

//        String display = "";
//        for (int i = 0; i < 7; i++) {
//            for (int j = 0; j < 7; j++) {
//                if (i % 2 == 0 ){
//                    System.out.printf(  (j %3 == 0) ? "#":"+" );
//                } else {
//                    System.out.printf("=");
//                }
//            }
//            System.out.println();
//        }

//        ex lisa 2
        String sisend = "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti; Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome; Eesnimi: Kalle, Perenimi: Kuul, Vanus: 38, Amet: arhitekt, Kodakondsus: Läti; Eesnimi: James, Perenimi: Cameron, Vanus: 56, Amet: riigiametnik, Kodakondsus: UK; Eesnimi: Donald, Perenimi: Trump, Vanus: 73, Amet: kinnisvaraarendaja, Kodakondsus: USA";
//        String[] sisendRead = sisend.split("; ");
//        String[][] rahvas = new String[sisendRead.length][5];
////        System.out.println(sisendRead[2]);
//
//        for (int i = 0; i < sisendRead.length; i++) {
////            System.out.println(sisendRead[i]);
//            String[] vahepealneArray = sisendRead[i].split(", ");
//
//            for (int j = 0; j < vahepealneArray.length; j++) {
//                vahepealneArray[j] = vahepealneArray[j].split(".*: ")[1];
//            }
//            rahvas[i] = vahepealneArray;
//        }
//
//        System.out.println(rahvas[1][1]);
//
//        for (String[] rida : rahvas) {
//            for (String kirje : rida) {
//                System.out.printf(kirje+ ", ");
//            }
//            System.out.println();
//        }

//        ex 2 variant 2

//        List<List<List<String>>> finaal = new ArrayList<>();
//        List<String> esimeneRing;
//
//        esimeneRing = Arrays.asList(sisend.split("; "));
//
//        for (int i = 0; i < esimeneRing.size(); i++) {
//            List<String> teineRing = Arrays.asList(esimeneRing.get(i).split(", "));
//            List<List<String>> teineListiRing = new ArrayList<>();
//
//            for (int j = 0; j < teineRing.size(); j++) {
//                List<String> kolmasRing = Arrays.asList(teineRing.get(j).split(": ")[1]);
//                teineListiRing.add(kolmasRing);
//            }
//
//            finaal.add(teineListiRing);
//        }
//
//        for (int i = 0; i < finaal.size(); i++) {
//            System.out.println(finaal.get(i));
//        }

//        ex2 variant 3

//        List<String> pilpad1 = Arrays.asList(sisend.split("; "));
//        List<List<Map<String, String>>> sektsioon = new ArrayList<>();
//
//        for (String tekst1 : pilpad1) {
//            List<String> pilpad2 = Arrays.asList(tekst1.split(", "));
//            List<Map<String, String>> gramm = new ArrayList<>();
//            for (String tekst2: pilpad2) {
//                Map<String, String> paarilised = new HashMap<>();
//                paarilised.put(tekst2.split(": ")[0], tekst2.split(": ")[1]);
//                gramm.add(paarilised);
//
//            }
//            sektsioon.add(gramm);
//        }
//
//        for (int i = 0; i < sektsioon.size(); i++) {
////            System.out.println(sektsioon.get(i));
//            for (int j = 0; j < sektsioon.get(i).size(); j++) {
//                for (String nameKey: sektsioon.get(i).get(j).values()) {
//                    System.out.printf("%s ", nameKey);
//                }
//            }
//            System.out.println();
//        }

        //proovi ise teha see nüüd nii, et järele jääks 2-mõõtmeline struktuur
        //key on eesnimi perenimi ja sisuks 3-kohaline stringide massiiv
//        Map demo:
//        ----------------------------
//                Kalle Kuul / 38 aastat / ametilt arhitekt / kodakondsus Läti
//                ----------------------------
//        Donald Trump / 73 aastat / ametilt kinnisvaraarendaja / kodakondsus USA
//                ----------------------------
//        Teet Kask / 34 aastat / ametilt lendur / kodakondsus Eesti
//                ----------------------------
//        Mari Tamm / 56 aastat / ametilt kosmonaut / kodakondsus Soome
//                ----------------------------
//        James Cameron / 56 aastat / ametilt riigiametnik / kodakondsus UK

        Map<String, List<String>> kodanikud = new TreeMap<>();

        for (String esimenePealeSisendit : sisend.split("; ")) {
//            System.out.println(esimenePealeSisendit);
            String[] teinePealeSisendit = esimenePealeSisendit.split(", ");
            for (int i = 0; i < teinePealeSisendit.length; i++) {
                teinePealeSisendit[i] = teinePealeSisendit[i].split(": ")[1];
//                System.out.println(teinePealeSisendit[i]);
            }
            List<String> kodanik = new ArrayList<>();
            String key = teinePealeSisendit[0] + "_" + teinePealeSisendit[1];
//            System.out.println(key);
            for (int i = 2; i < teinePealeSisendit.length; i++) {
//                System.out.println(teinePealeSisendit[i]);
                kodanik.add(teinePealeSisendit[i]);
            }
            kodanikud.put(key, kodanik);

        }
//            System.out.println(kodanikud);
        for (String kirje : kodanikud.keySet()) {
//            System.out.println(kodanikud.get(kirje).get(0));
            System.out.printf("%s", kirje);
//            System.out.println(kodanikud.size()-1);
            for (int i = 0; i < kodanikud.size()-2; i++) {
                System.out.printf(" / %s",kodanikud.get(kirje).get(i));
            }
            System.out.println();



        }



    }
}





















