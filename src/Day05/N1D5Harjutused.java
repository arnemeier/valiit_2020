package Day05;

import java.awt.image.AreaAveragingScaleFilter;
import java.lang.reflect.Array;
import java.util.*;

public class N1D5Harjutused {
    public static void main(String[] args) {

//        ex 1
//        for (int k = 6; k > 0; k--) {
//            for (int i = k; i > 0; i--) {
//                System.out.print("#");
//            }
//            System.out.println();
//        }


//        ex 2
//        for (int k = 0; k < 6; k++) {
//            for (int i = 0; i <= k; i++) {
//                System.out.printf("#");
//            }
//            System.out.println();
//        }

//        ex 3

//        for (int i = 4; i >= 0; i--) {
////            System.out.print("i: " + i);
//            for (int k = 5-i; k < 5; k++) {
//                System.out.print(" ");
//            }
//            for (int j = i; j < 5; j++) {
//                System.out.print("@");
//            }
//            System.out.println();
//        }

//        ex4

//        int number = 43247216;
//        String tekst = String.valueOf(number);
//        String uusTekst = "";
//
//        for (int i = tekst.length()-1; i >= 0; i--) {
//            uusTekst += tekst.charAt(i);
//        }
//        System.out.println(Integer.parseInt(uusTekst));

//        ex4 v2, stringbuilder
//        String tekst = "kuulilennuteetunneliluuk";
//        StringBuilder sb = new StringBuilder();
//        sb.append(tekst);
//        System.out.println(String.valueOf(sb.reverse()));


//        ex5
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Sisesta palun oma nimi:");
//        String nimi = scanner.nextLine();
//        System.out.println("Sisesta palun oma eksami tulemus:");
//        String tulemus = scanner.nextLine();
//        int tulemusNumbrina = Integer.parseInt(tulemus);

//            variant 2, vaja oli args-ga
//            String nimi = args[0];
//            int tulemusNumbrina = Integer.parseInt(args[1]);


//        if (tulemusNumbrina > 91 && tulemusNumbrina < 100) {
//            System.out.println(nimi + " : PASS - 5, " + tulemus );
//        } else if (tulemusNumbrina > 81 && tulemusNumbrina < 90){
//            System.out.println(nimi + " : PASS - 4, " + tulemus );
//        } else if (tulemusNumbrina > 71 && tulemusNumbrina < 80){
//            System.out.println(nimi + " : PASS - 3, " + tulemus );
//        } else if (tulemusNumbrina > 61 && tulemusNumbrina < 70){
//            System.out.println(nimi + " : PASS - 2, " + tulemus );
//        } else if (tulemusNumbrina > 51 && tulemusNumbrina < 60){
//            System.out.println(nimi + " : PASS - 1, " + tulemus );
//        } else if (tulemusNumbrina > 0 && tulemusNumbrina < 50){
//            System.out.println(nimi + " : FAIL" );
//        } else {
//            System.out.println("imelik number, sisesta number 1-100");
//        }

//        ex6

//        double[][] massiiv = {{1.55, 2.45},{1.12, 3.55},{0.48, 5.12},{0.49,3.14}};
//        for (double[] paar : massiiv) {
//            double ruutudeSumma = 0;
//            for (double arv : paar) {
//                ruutudeSumma += Math.pow(arv, 2);
//            }
//            System.out.println(Math.sqrt(ruutudeSumma));
//        }

//        ex7

//        String[][] riigid = {
//                {"Estonia", "Tallinn", "Jüri Ratas"},
//                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
//                {"Lithuania", "Vilnius", "Saulius Skvernelis"},
//                {"Finland", "Helsinki", "Sanna Marin"},
//                {"Sweden", "Stockholm", "Stefan Löfven"},
//                {"Norway", "Oslo", "Erna Solberg"},
//                {"Denmark", "Copenhagen", "Mette Frederiksen"},
//                {"Russia", "Moscow", "Mikhail Mishustin"},
//                {"Germany", "Berlin", "Angela Merkel"},
//                {"France", "Paris", "Édouard Philippe"}
//        };

//        for (String[] riik : riigid) {
//            System.out.println(riik[2]);
//        }
//        for (String[] riik : riigid) {
//            System.out.println("Country: " + riik[0] + ", Capital: " + riik[1] + ", Prime minister: " + riik[2]);
//        }

//        ex8

        String[][][] riigid = {
                {{"Estonia", "Tallinn", "Jüri Ratas"}, {"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"}},
                {{"Latvia", "Riga", "Arturs Krišjānis Kariņš"}, {"Latvian", "Russian", "Belarusian", "Ukrainian", "Polish"}},
                {{"Lithuania", "Vilnius", "Saulius Skvernelis"}, {"lithuanian"}},
                {{"Finland", "Helsinki", "Sanna Marin"}, {"finnish"}},
                {{"Sweden", "Stockholm", "Stefan Löfven"}, {"swedish"}},
                {{"Norway", "Oslo", "Erna Solberg"}, {"norwegian"}},
                {{"Denmark", "Copenhagen", "Mette Frederiksen"}, {"danish"}},
                {{"Russia", "Moscow", "Mikhail Mishustin"}, {"russian"}},
                {{"Germany", "Berlin", "Angela Merkel"}, {"german"}},
                {{"France", "Paris", "Édouard Philippe"}, {"french"}}
        };

//        for (String[][] riigikomplekt : riigid) {
//            System.out.println(String.format("%s / %s / %s :", riigikomplekt[0][0], riigikomplekt[0][1], riigikomplekt[0][2]));
//            for (String keel : riigikomplekt[1]) {
//                System.out.println(String.format("    %s", keel));
//            }
//        }

//        ex9
//        String[][] testiks = {{"Estonia", "Tallinn", "Jüri Ratas"}, {"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"}};

//        List<List<List<String>>> riigikomplektid = new ArrayList<>();
//        for (String[][] komplekt : riigid) {
//            List<List<String>> teineTasand = new ArrayList<>();
//            for (String[] riigiKaksKirjet : komplekt) {
////                System.out.println();
//                List<String> kolmasTasand = new ArrayList<>();
//                for (String andmed : riigiKaksKirjet) {
//                    kolmasTasand.add(andmed);
//                }
//                teineTasand.add(kolmasTasand);
//            }
//            riigikomplektid.add(teineTasand);
////            System.out.println();
//        }
////        System.out.println(riigikomplektid);
//
//        for (List<List<String>> lev1 : riigikomplektid) {
////            System.out.println(lev1);
//            for (List<String> lev2 : lev1) {
//                for (String lev3 : lev2) {
////                    System.out.println(lev3);
//
//                }
//
//            }
//        }

//        for (int i = 0; i < riigikomplektid.size(); i++) {
//            System.out.println(riigikomplektid.get(0).get(0).get(0));
//            System.out.println(String.format("%s / %s / %s:", riigikomplektid.get(i).get(0).get(0), riigikomplektid.get(i).get(0).get(1), riigikomplektid.get(i).get(0).get(2)));
//            System.out.println("___");
//            int param = riigikomplektid.get(i).get(1).size();
//            System.out.println(param);
//            System.out.println(riigikomplektid.get(i).get(1).get(0));
//            for (int j = 0; j < param; j++) {
//                System.out.println("--");
//                System.out.println(riigikomplektid.get(i).get(1).get(j));
//            }
//        }


//        ex10

//        Map<String, String[][]> lander = new TreeMap<>();
//        lander.put(
//                "Estonia", new String[][]{{"Tallinn", "Jüri Ratas"}, {"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"}}
//        );
//        lander.put(
//                "Latvia", new String[][]{{"Riga", "Arturs Krišjānis Kariņš"}, {"Latvian", "Russian", "Belarusian", "Ukrainian", "Polish"}}
//        );
//        lander.put(
//                "Lithuania", new String[][]{{"Vilnius", "Saulius Skvernelis"}, {"lithuanian"}}
//        );
//        lander.put(
//                "Finland", new String[][]{{"Helsinki", "Sanna Marin"}, {"finnish"}}
//        );
//
//        for (String landName : lander.keySet() ) {
////            System.out.println();
//            System.out.printf("%s / %s / %s", landName, lander.get(landName)[0][0], lander.get(landName)[0][1]);
//            System.out.println();
//            for (String sprache: lander.get(landName)[1]) {
//
//                System.out.println(sprache);
//            }
//
//
//        }

//        ex11

//        Queue<String[][]> riigidQ = new LinkedList<>();
//
//        for (String[][] riikQ: riigid) {
//            riigidQ.add(riikQ);
//        }

//        for (String[][] riikReadQ : riigidQ) {
//            System.out.printf("%s / %s / %s", riikReadQ[0][0], riikReadQ[0][1], riikReadQ[0][2]);
//            System.out.println();
//        }

//        riigidQ.forEach(strings -> System.out.println(strings[0][0]));
//
//        Queue<String> testiks = new LinkedList<>();
//        testiks.add("asdf");
//        testiks.forEach(asdf -> System.out.println(asdf));

//        while (!riigidQ.isEmpty()){
//            String[][] country = riigidQ.poll();
//            String countryName = country[0][0];
//            String countryCapital = country[0][1];
//            String primeMinister = country[0][2];
//            String[] countryLangs = country[1];
//
//            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, primeMinister);
//            for (String lang : countryLangs) {
//                System.out.println("\t"+ lang);
//
//            }
//        }


    }
}