package BankBeta;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class BankApp {
    public static void main(String[] args) throws IOException {

        AccountService accS = new AccountService();
        accS.loadAccounts("/Users/arne/dev/HelloWorldApp/Resources/kontod.txt");

//        System.out.println(accS.searchAccount("758818991"));
//
//        if (accS.transfer("318129395", "758818991", 8)) {
//            System.out.println("success!");
//        } else {
//            System.out.println("problem");
//        }

//        System.out.println(accS.searchAccount("758818991"));
//        System.out.println(accS.searchAccount("318129395"));

        System.out.println("##############################################");
        System.out.println("#              TÜRISALU PANK                 #");
        System.out.println("#                                            #");
        System.out.println("# SYNTAX:                                    #");
        System.out.println("# BALANCE <ACCOUNT>                          #");
        System.out.println("# BALANCE <FIRST NAME> <LAST NAME>           #");
        System.out.println("# TRANSFER <FROM ACCOUNT> <TO ACCOUNT> <SUM> #");
        System.out.println("#                                            #");
        System.out.println("##############################################");
        System.out.printf("Insert choice: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        while (!input.toUpperCase().equals("EXIT")) {
            String[] words = input.split(" ");
            if (words.length == 2) {
                if (words[0].toUpperCase().equals("BALANCE")) {
                    String balanceNumberResult = accS.searchAccount(words[1]);
                    System.out.println(balanceNumberResult);
                    System.out.printf("Insert choice: ");
                    input = scanner.nextLine();
                }
            }
            else if (words.length == 3) {
                if (words[0].toUpperCase().equals("BALANCE")) {
                    String nameResult = accS.searchAccount(words[1] + " " + words[2]);
                    System.out.println(nameResult);
                    System.out.printf("Insert choice: ");
                    input = scanner.nextLine();
                }
            }
            else if (words.length == 4){
                if (words[0].toUpperCase().equals("TRANSFER")) {

                    if (accS.transfer(words[1],words[2], Integer.parseInt(words[3]))){
                        System.out.println("Transfer successful!");
                    } else {
                        System.out.println("Transfer failed: insufficient fonds / wrong acc no!");
                    }
                    System.out.printf("Insert choice: ");
                    input = scanner.nextLine();
                }
            } else {
                System.out.printf("Insert choice: ");
                input = scanner.nextLine();
            }

        }
        System.out.println("Thank you, cya!");
    }
}
