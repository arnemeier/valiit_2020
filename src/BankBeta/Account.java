package BankBeta;

public class Account {
    private String eesnimi;
    private String perenimi;
    String kontonumber;
    int balance;

    public String getEesnimi() {
        return eesnimi;
    }

    public void setEesnimi(String eesnimi) {
        this.eesnimi = eesnimi;
    }

    public String getPerenimi() {
        return perenimi;
    }

    public void setPerenimi(String perenimi) {
        this.perenimi = perenimi;
    }

    public String getKontonumber() {
        return kontonumber;
    }

    public void setKontonumber(String kontonumber) {
        this.kontonumber = kontonumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "\nAccount{" +
                "Nimi='" + eesnimi + " " + perenimi + '\'' +
                ", kontonumber='" + kontonumber + '\'' +
                ", balance=" + balance +
                '}';
    }
}
