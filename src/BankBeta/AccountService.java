package BankBeta;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AccountService {
    private static List<Account> accounts;

    static List<Account> loadAccounts(String accountFilePath) throws IOException {
        accounts = new ArrayList<>();
        List<String> info = Files.readAllLines(Paths.get(accountFilePath));
        for (String inf : info) {
            Account a1 = new Account();
            a1.setEesnimi(inf.split(", ")[0]);
            a1.setPerenimi(inf.split(", ")[1]);
            a1.setKontonumber(inf.split(", ")[2]);
            a1.setBalance(Integer.parseInt(inf.split(", ")[3]));
            accounts.add(a1);
        }
        return accounts;
    }

    static String searchAccount(String input) {
        for (Account acc : accounts) {
            String nameSearch = acc.getEesnimi() + " " + acc.getPerenimi();
            if (input.equals(nameSearch) || input.equals(acc.getKontonumber())) {
                return String.format("%s %s, %s, %d", acc.getEesnimi(), acc.getPerenimi(), acc.getKontonumber(), acc.getBalance());
            }
        }
        return "No match!";
    }


    static boolean transfer(String fromAccount, String toAccount, int sum) {
        for (Account acc : accounts) {
            if (acc.kontonumber.equals(fromAccount) && acc.getBalance() >= sum) {
                for (Account acc2 : accounts) {
                    if (acc2.kontonumber.equals(toAccount)) {
                        acc.balance -= sum;
                        acc2.balance += sum;
                        return true;
                    }
                }
            }
        }
        return false;
    }

}