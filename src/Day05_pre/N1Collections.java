package Day05_pre;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class N1Collections {
    public static void main(String[] args) {
        List asi = new ArrayList();
        asi.add(5);
        asi.add(7);
        asi.add(1);
        int as = (int)asi.get(2);
        System.out.println(as);

        Set<String> nimed = new HashSet <>();
        nimed.add("esimene");
        nimed.add("teine");
        nimed.add("kolmas");
        System.out.println(nimed);

//        NB! nii ei saa mitte!!!
//        System.out.println(nimed[0]);
    }
}
