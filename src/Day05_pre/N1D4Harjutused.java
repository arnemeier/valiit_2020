package Day05_pre;

import java.util.*;
import java.util.stream.Stream;

public class N1D4Harjutused {
    public static void main(String[] args) {
//        ex19
//        List<String> linnad = new ArrayList<String>();
//        linnad.add("Tallinn");
//        linnad.add("Tartu");
//        linnad.add("Viljandi");
//        linnad.add("Pärnu");
//        linnad.add("Narva");
//        System.out.println(linnad.get(0));
//        System.out.println(linnad.get(2));
//        System.out.println(linnad.get(linnad.size()-1));

        //ex20
//        Queue<String> elemendid = new LinkedList<>();
//        elemendid.add("nikkel");
//        elemendid.add("kaadmium");
//        elemendid.add("uraan");
//        elemendid.add("koobalt");
//        elemendid.add("hõbe");
//        elemendid.add("vask");
//
//        int index = elemendid.size();
//
//        while (elemendid.size() > 0){
//            System.out.println(elemendid.remove());
//        }

//        ex21

//        Set<String> esimeneSet = new TreeSet<>();
//        esimeneSet.add("6:1");
//        esimeneSet.add("6:2");
//        esimeneSet.add("6:7");
//        esimeneSet.add("3:6");
//        esimeneSet.add("7:5");
//
//        esimeneSet.forEach(name -> System.out.println(name));
////        on sama mis
//        esimeneSet.forEach(System.out::println);

//        ex22

        Map<String, String[]> riigidLinnad = new HashMap<>();
        String[] eestiLinnad = {"Tallinn", "Tartu", "Valga", "Võru"};
        String[] rootsiLinnad = {"Stockholm", "Uppsala", "Lund", "Köping"};
        String[] soomeLinnad = {"Helsinki", "Espoo", "Hanko", "Jämsä"};
        riigidLinnad.put("Estonia", eestiLinnad);
        riigidLinnad.put("Sweden", rootsiLinnad);
        riigidLinnad.put("Finland", soomeLinnad);

//        for (String maa : riigidLinnad.keySet()) {
//            System.out.println("Country: " + maa);
//            System.out.println("Cities:");
//            for (String linn : riigidLinnad.get(maa)) {
//                System.out.println("\t"+linn);
//            }
//        }

        riigidLinnad.forEach((maa, linn) -> {
            System.out.println("Countries:" + maa);
            System.out.println("Cities");
            Stream.of(linn).forEach(m-> System.out.println("##\t"+ m));
        });


    }
}

